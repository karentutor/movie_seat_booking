const container = document.querySelector('.container');
const seats = document.querySelectorAll('.row .seat:not(.occupied)'); /* a node list */
const count = document.getElementById('count');
const total = document.getElementById('total');
const movieSelect = document.getElementById('movie');

populateUI();

// make into a number by adding a plus sign
let ticketPrice = +movieSelect.value;




//functions
function setMovieData(movieIndex, moviePrice)
{
    localStorage.setItem('selectedMovieIndex', movieIndex); // not an array so no JSON.stringify
    localStorage.setItem('selectedMoviePrice', moviePrice);
}



function updateSelectedCount() {
    
    // puts all of our selected seats into a node list
    const selectedSeats = document.querySelectorAll('.row .seat.selected');
    //console.log(selectedSeats);

    //Copy selected seats into an array
    // Map through that array return a new array of indexes
    //const seatsIndex = [...selectedSeats];// copies the elements of an array
    //console.log(seatsIndex);
    //return;

    // the magic -- gets the index of the chosen seats and creates and array of this
    const seatsIndex = [...selectedSeats].map(seat => [...seats].indexOf(seat));
    // put into local storage
    localStorage.setItem('selectedSeats', JSON.stringify(seatsIndex));

    const selectedSeatsCount = selectedSeats.length;
    //console.log(selectedSeatsCount);
    count.innerText = selectedSeatsCount;
    total.innerText = selectedSeatsCount * ticketPrice;
}

//get data from local storage and populate  UI
function populateUI() {

    // Firt thing is to get the seats from local stroage
    //now need to put back into an array -- opposite of json.stringify
    const selectedSeats = JSON.parse(localStorage.getItem('selectedSeats'));// 
    //console.log(selectedSeats)
    
    //check to see if anything in the selected seats
    // check to see if local storage is null (i.e. exit and lenght > 0)
    if (selectedSeats !== null && selectedSeats.length > 0)
    {
        seats.forEach((seat,index) => {
            // this means that if selected seats in is in the array seats index of  -- not there is -1
            // if greater than -1 then it is there
            if(selectedSeats.indexOf(index) > -1)
            {
                // interesting here is how we can concatenate onto the end of the class list
                seat.classList.add('selected');
            }
        });
    }

    const selectedMovieIndex = localStorage.getItem('selectedMovieIndex');

    if (selectedMovieIndex !== null) {
        movieSelect.selectedIndex = selectedMovieIndex;
    }

}

//Movie select event
movieSelect.addEventListener('change', e => {
    ticketPrice = +e.target.value;
    // here save the index movie
    //console.log(e.target.selectedIndex);
    // here save the value
    //console.log(e.target.value);
    updateSelectedCount();
    setMovieData(e.target.selectedIndex, e.target.value);
})

// seat click event
//use container instead of individual node items
container.addEventListener('click', (e) => {
    //checks to see if the class contains a word
    if(e.target.classList.contains('seat') && !e.target.classList.contains('occupied')) 
    {
        // clast list add. remove and toggle
        e.target.classList.toggle('selected');
        updateSelectedCount();
    }
});

//Initial count and total set
updateSelectedCount();